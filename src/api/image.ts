
import { netGet, netPost } from '@/utils/request'
// 获取平台图片分类
export function getImageType() {
  return netGet('/files/type')
}
// 获取平台图片分类下的图片
export function getImageList(params: any) {
  return netPost('/admin/space/info/page',params)
}
// 获取系统的图片
export function getImages(params : any) {
  return netPost('/files/select/page', params)
}
//
export function saveProjectFile(data: any) {
  return netPost('/app/base/comm/screen/file/add', data)
}

export function deleteProjectFile(data: any) {
  return netPost('/files/file/deletes', { ids: data })
}


