/*
 * @Author: '张朋朋' '14065467+a598001989@user.noreply.gitee.com'
 * @Date: 2024-03-27 14:11:29
 * @LastEditors: '张朋朋' '14065467+a598001989@user.noreply.gitee.com'
 * @LastEditTime: 2024-04-17 15:30:16
 * @FilePath: \iking-datav\src\api\qiniu.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import axios, { AxiosRequestConfig } from 'axios'

export function upload(url: string, data: any, config?: AxiosRequestConfig) {
  return axios.create().post(url, data, {
    withCredentials: false,
    ...(config || {}),
  })
}

export async function getTokenByEnv(): Promise<string> {
  try {
    const TokenKey = import.meta.env.VITE_DATAV_TOKEN_KEY
    const token = localStorage.getItem(TokenKey)
    return token
  } catch (error) {
    throw error
  }
}
