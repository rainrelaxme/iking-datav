/*
 * @Author: root zhangpengpeng@ikingtech.com
 * @Date: 2024-03-18 10:47:16
 * @LastEditors: root zhangpengpeng@ikingtech.com
 * @LastEditTime: 2024-03-21 18:51:51
 * @FilePath: \iking-datav-new\src\components\components-map\map\china2d\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import type { App } from 'vue'
import type { SFCWithInstall } from '@/utils/types'
import { loadAsyncComponent } from '@/utils/async-component'
import China2d from './src/index.vue'
import China2dArea from './src/china2d-area/index.vue'
import China2dBubbles from './src/china2d-bubbles/index.vue'
import China2dFlyingline from './src/china2d-flyingline/index.vue'

China2d.install = (app: App): void => {
  app.component('VChina2d', China2d)
  app.component('VChina2dArea', China2dArea)
  app.component('VChina2dBubbles', China2dBubbles)
  app.component('VChina2dFlyingline', China2dFlyingline)

  app.component('VChina2dProp', loadAsyncComponent(() => import('./src/config.vue')))
  app.component('VChina2dAreaProp', loadAsyncComponent(() => import('./src/china2d-area/config.vue')))
  app.component('VChina2dBubblesProp', loadAsyncComponent(() => import('./src/china2d-bubbles/config.vue')))
  app.component('VChina2dFlyinglineProp', loadAsyncComponent(() => import('./src/china2d-flyingline/config.vue')))
}

export default China2d as SFCWithInstall<typeof China2d>
