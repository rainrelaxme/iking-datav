/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-01-11 16:24:05
 * @LastEditTime : 2024-01-11 16:25:28
 */

import { ComDataType } from '@/data/system-components'

const COM_CDN = ''

export const itemList: ComDataType = {
  type: 'title',
  name: '列表',
  icon: 'v-icon-title',
  data: [
    {
      name: 'VScrollArticle',
      alias: '轮播列表',
      img: `${COM_CDN}/datav/file/com-picture/scroll-article.png`,
      thum: `${COM_CDN}/datav/file/com-picture/scroll-article.png`,
      used: true,
    },
  ],
}
