/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-12-13 19:00:42
 * @LastEditTime : 2023-12-14 13:38:51
 */
import { ComDataType } from '@/data/system-components'
const COM_CDN = ''
export const model: ComDataType = {
  type: 'model',
  name: '模型',
  icon: 'v-icon-chart-line',
  data: [
    {
      name: 'VModelTitles',
      alias: '3DTitles模型',
      img: `${COM_CDN}/datav/file/com-picture/model.png`,
      thum: `${COM_CDN}/datav/file/com-picture/model.png`,
      used: true,
    }, {
      name: 'VModelGltf',
      alias: 'GLTF模型',
      img: `${COM_CDN}/datav/file/com-picture/model.png`,
      thum: `${COM_CDN}/datav/file/com-picture/model.png`,
      used: false,
    },
  ],
}
