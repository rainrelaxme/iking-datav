
/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-12-11 18:08:49
 * @LastEditTime : 2024-02-01 11:31:37
 */
import { ComDataType } from '../system-components'
const COM_CDN = ''
export const title: ComDataType = {
  type: 'title',
  name: '标题',
  icon: 'v-icon-title',
  data: [
    {
      name: 'VPowerHtml',
      alias: '自定义HTML',
      img: `${COM_CDN}/datav/file/com-picture/custom-html-332-144.jpg`,
      thum: `${COM_CDN}/datav/file/com-picture/custom-html-332-144.jpg`,
      used: true,
    },
    {
      name: 'VTimer',
      alias: '时间器',
      img: `${COM_CDN}/datav/file/com-picture/timer-160-116.png`,
      thum: `${COM_CDN}/datav/file/com-picture/timer-370-208.png`,
      used: true,
    },
    {
      name: 'VMainTitle',
      alias: '通用标题',
      img: `${COM_CDN}/datav/file/com-picture/main-title-332-144.png`,
      thum: `${COM_CDN}/datav/file/com-picture/main-title-370-208.png`,
      used: true,
    }, {
      name: 'VNumberTitleFlop',
      alias: '数字翻牌器',
      img: `${COM_CDN}/datav/file/com-picture/number-title-flop-160-116.png`,
      thum: `${COM_CDN}/datav/file/com-picture/number-title-flop-370-208.png`,
      used: true,
    }, {
      name: 'VWeatherApi',
      alias: '天气(外部API)',
      img: `${COM_CDN}/datav/file/com-picture/weather-api.png`,
      used: true,
    }, {
      name: 'VNumberFlip',
      alias: '翻牌器(纯数字)',
      img: `${COM_CDN}/datav/file/com-picture/number-flip.png`,
      used: true,
    }, {
      name: 'VMarquee',
      alias: '跑马灯',
      img: `${COM_CDN}/datav/file/com-picture/marquee-332-144.png`,
      thum: `${COM_CDN}/datav/file/com-picture/marquee-370-208.png`,
      used: true,
    }, {
      name: 'VParagraph',
      alias: '多行文本',
      img: `${COM_CDN}/datav/file/com-picture/paragraph-160-116.png`,
      thum: `${COM_CDN}/datav/file/com-picture/paragraph-370-208.png`,
      used: true,
    },
  ],
}
