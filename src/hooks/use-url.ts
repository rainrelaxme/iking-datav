
const { VITE_APP_BASE_API, VITE_UPLOAD_URL, VITE_LOAD_URL } = import.meta.env

export const useEnvUrl = () => {
  const VITE_APP_CDN = (window as any).ikBasicsUrl
  const setUrl = (url: string) => {
    return url ? url.startsWith('http') ? url : `${VITE_APP_CDN}${url.startsWith('/') ? url : `/${url}`}` : url
  }
  return {
    baseUrl: VITE_APP_BASE_API,
    uploadUrl: VITE_UPLOAD_URL,
    loadUrl: VITE_LOAD_URL,
    cdnUrl: VITE_APP_CDN,
    setUrl,
  }
}
